<?php
/**
 * crawlerdetect plugin for Craft CMS 3.x
 *
 * Use CrawlerDetect with Craft
 *
 * @link      https://www.adrienpicard.co.uk
 * @copyright Copyright (c) 2018 Adrien Picard
 */

namespace apstudio\crawlerdetect;

use apstudio\crawlerdetect\services\CrawlerdetectService as CrawlerdetectServiceService;
use apstudio\crawlerdetect\variables\CrawlerdetectVariable;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\twig\variables\CraftVariable;

use yii\base\Event;

/**
 * Class Crawlerdetect
 *
 * @author    Adrien Picard
 * @package   Crawlerdetect
 * @since     1.0.0
 *
 * @property  CrawlerdetectServiceService $crawlerdetectService
 */
class Crawlerdetect extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var Crawlerdetect
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init(){

        parent::init();
        self::$plugin = $this;

        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('crawlerdetect', CrawlerdetectVariable::class);
            }
        );

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Craft::info(
            Craft::t(
                'crawlerdetect',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
