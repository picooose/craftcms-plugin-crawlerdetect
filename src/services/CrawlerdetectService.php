<?php
/**
 * crawlerdetect plugin for Craft CMS 3.x
 *
 * Use CrawlerDetect with Craft
 *
 * @link      https://www.adrienpicard.co.uk
 * @copyright Copyright (c) 2018 Adrien Picard
 */

namespace apstudio\crawlerdetect\services;

use apstudio\crawlerdetect\Crawlerdetect;
use Jaybizzle\CrawlerDetect\CrawlerDetect as CD;

use Craft;
use craft\base\Component;

/**
 * @author    Adrien Picard
 * @package   Crawlerdetect
 * @since     1.0.0
 */
class CrawlerdetectService extends Component
{
    // Public Methods
    // =========================================================================

    /*
     * @return mixed
     */
    public function isCrawler(){
        $CrawlerDetect = new CD;
        return $CrawlerDetect->isCrawler();
    }
}
