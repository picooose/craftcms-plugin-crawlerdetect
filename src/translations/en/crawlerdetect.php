<?php
/**
 * crawlerdetect plugin for Craft CMS 3.x
 *
 * Use CrawlerDetect with Craft
 *
 * @link      https://www.adrienpicard.co.uk
 * @copyright Copyright (c) 2018 Adrien Picard
 */

/**
 * @author    Adrien Picard
 * @package   Crawlerdetect
 * @since     1.0.0
 */
return [
    'crawlerdetect plugin loaded' => 'crawlerdetect plugin loaded',
];
