<?php
/**
 * crawlerdetect plugin for Craft CMS 3.x
 *
 * Use CrawlerDetect with Craft
 *
 * @link      https://www.adrienpicard.co.uk
 * @copyright Copyright (c) 2018 Adrien Picard
 */

namespace apstudio\crawlerdetect\variables;

use apstudio\crawlerdetect\Crawlerdetect;
use Craft;

/**
 * @author    Adrien Picard
 * @package   Crawlerdetect
 * @since     1.0.0
 */
class CrawlerdetectVariable
{
    // Public Methods
    // =========================================================================

    /**
     * @param null $optional
     * @return string
     */
    public function bot($optional = null){
        return Crawlerdetect::$plugin->crawlerdetectService->isCrawler();
    }
}
